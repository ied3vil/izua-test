import Vue from 'vue'
import Izua from './Izua'

const app = new Vue({
  el: '#app',
  components: {
    Izua,
  },
  data: {
    name: 'Izua'
  },
  methods: {
    sayMyName() {
      this.name = 'Gogu';
    }
  }
})

